#!/bin/bash

# Slurm job options (job-name, compute nodes, job time)
#SBATCH --job-name=Example_MPI_Job
#SBATCH --time=0:29:59
#SBATCH --nodes=1
#SBATCH --tasks-per-node=128
#SBATCH --cpus-per-task=1

# Replace [budget code] below with your budget code (e.g. t01)
#SBATCH --account=c01-mol
#SBATCH --partition=standard
#SBATCH --qos=standard

# Setup the job environment (this module needs to be loaded before any other modules)
module load epcc-job-env
module load cray-fftw

# Set the number of threads to 1
#   This prevents any threaded system libraries from automatically 
#   using threading.
export OMP_NUM_THREADS=1

# Launch the parallel job
#   Using 512 MPI processes and 128 MPI processes per node
#   srun picks up the distribution from the sbatch options

#cd /work/c01/c01/oxijb/bmark/MD/NaCl-lj/dlp
#for i in `seq 11 20`
#do
    cp REVCON NaCl.config
    cp REVCON NaCl.config.run
    srun --distribution=block:block --hint=nomultithread ./DLPOLY.Z > out.$i
    rm core
#done

